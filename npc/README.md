Most of the token pictures are generated with artflow.ai.
The token frame is made by me, so you can use it as you wish.
I still use images found over the internet for some tokens. If you see your art and don't want it there, send me a message and I'll take it down.